package com.xtdp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableAspectJAutoProxy(exposeProxy = true)
@MapperScan("com.xtdp.mapper")
@SpringBootApplication
public class XTDianPingApplication {

    public static void main(String[] args) {
        SpringApplication.run(XTDianPingApplication.class, args);
    }

}
