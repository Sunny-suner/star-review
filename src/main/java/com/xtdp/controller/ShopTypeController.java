package com.xtdp.controller;


import com.xtdp.dto.Result;
import com.xtdp.service.IShopTypeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
@RequestMapping("/shop-type")
public class ShopTypeController {
    @Resource
    private IShopTypeService typeService;


    @GetMapping("list")
    public Result queryTypeList() {
        //List<ShopType> typeList = typeService.query().orderByAsc("sort").list();
        return typeService.queryByList();
    }
}
