package com.xtdp.service;

import com.xtdp.dto.Result;
import com.xtdp.entity.ShopType;
import com.baomidou.mybatisplus.extension.service.IService;


public interface IShopTypeService extends IService<ShopType> {

    Result queryByList();
}
