package com.xtdp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xtdp.dto.LoginFormDTO;
import com.xtdp.dto.Result;
import com.xtdp.entity.User;

import javax.servlet.http.HttpSession;


public interface IUserService extends IService<User> {

    Result sendCode(String phone, HttpSession session);

    Result login(LoginFormDTO loginForm, HttpSession session);

    Result sign();

    Result signCount();

}
