package com.xtdp.service;

import com.xtdp.entity.BlogComments;
import com.baomidou.mybatisplus.extension.service.IService;


public interface IBlogCommentsService extends IService<BlogComments> {

}
