package com.xtdp.service;

import com.xtdp.dto.Result;
import com.xtdp.entity.Follow;
import com.baomidou.mybatisplus.extension.service.IService;


public interface IFollowService extends IService<Follow> {

    Result follow(Long followUserId, Boolean isFollow);


    Result isFollow(Long followUserId);

    Result followCommons(Long id);
}
