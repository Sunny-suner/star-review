package com.xtdp.service.impl;

import com.xtdp.entity.SeckillVoucher;
import com.xtdp.mapper.SeckillVoucherMapper;
import com.xtdp.service.ISeckillVoucherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


@Service
public class SeckillVoucherServiceImpl extends ServiceImpl<SeckillVoucherMapper, SeckillVoucher> implements ISeckillVoucherService {

}
