package com.xtdp.service.impl;

import com.xtdp.entity.UserInfo;
import com.xtdp.mapper.UserInfoMapper;
import com.xtdp.service.IUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements IUserInfoService {

}
