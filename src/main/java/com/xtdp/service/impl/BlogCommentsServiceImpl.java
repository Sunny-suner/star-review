package com.xtdp.service.impl;

import com.xtdp.entity.BlogComments;
import com.xtdp.mapper.BlogCommentsMapper;
import com.xtdp.service.IBlogCommentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


@Service
public class BlogCommentsServiceImpl extends ServiceImpl<BlogCommentsMapper, BlogComments> implements IBlogCommentsService {

}
