package com.xtdp.utils;

public interface ILock {
    //获取锁（持有过期时间，过期后自动释放）
    boolean tryLock(long timeoutSec);


    //释放锁
    void unLock();


}
