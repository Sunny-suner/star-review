package mapper.mapper;

import com.xtdp.entity.Follow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface FollowMapper extends BaseMapper<Follow> {

}
