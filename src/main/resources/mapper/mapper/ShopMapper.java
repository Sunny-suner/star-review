package mapper.mapper;

import com.xtdp.entity.Shop;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface ShopMapper extends BaseMapper<Shop> {

}
