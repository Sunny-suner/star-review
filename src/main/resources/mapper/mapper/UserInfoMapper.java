package mapper.mapper;

import com.xtdp.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
