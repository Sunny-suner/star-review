package mapper.mapper;

import com.xtdp.entity.Blog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface BlogMapper extends BaseMapper<Blog> {

}
